package com.example.viewpager.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.viewpager.R
import com.example.viewpager.Url
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.database.FirebaseDatabase

class UrlFragment : Fragment(R.layout.fragment_url) {

    private val database = FirebaseDatabase.getInstance().getReference("Url")

    private lateinit var imageView : ImageView
    private lateinit var editTextUrl : EditText
    private lateinit var buttonSub : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        buttonSub.setOnClickListener {
            val url = editTextUrl.text.toString()

            val urlInsert = Url(url)

            database.setValue(urlInsert)

            Glide.with(this@UrlFragment).load(urlInsert.url).placeholder(R.drawable.ic_launcher_foreground)
                .into(imageView)

        }
    }

}