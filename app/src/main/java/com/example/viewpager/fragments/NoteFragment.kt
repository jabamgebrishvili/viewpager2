package com.example.viewpager.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpager.R

class NoteFragment : Fragment(R.layout.fragment_note) {
    private lateinit var textView : TextView
    private lateinit var buttonAdd : Button
    private lateinit var editTextNote : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //val sharedPreference =  getSharedPreferences()

        //val sharedPreferences = this.activity.getSharedPreferences("MY_APP_PR", context.Mode_Private)

        //val sharedPreferences = activity!!.applicationContext.getSharedPreferences("viewPager", context.MODE_PRIVATE)

        //val sharedPreferences = activity.applicationContext.getSharedPreferences("viewPager", 0)

        buttonAdd.setOnClickListener {
            val note = editTextNote.text.toString()
            val text = textView.text.toString()

            val result = note + "\n" + text
            textView.text = result
        }
    }
}