package com.example.viewpager.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpager.fragments.ImageFragment
import com.example.viewpager.fragments.NoteFragment
import com.example.viewpager.fragments.UrlFragment

class ViewPagerFragmentAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {

    override fun getItemCount() = 3


    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> UrlFragment()
            1 -> ImageFragment()
            2 -> NoteFragment()
            else -> UrlFragment()
        }
    }
}